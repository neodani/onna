# Wordpress in K8S

1. [Overview](#overview)
1. [Deliverable](#deliverable)
1. [Quick-Start](#quick-start)
1. [Endpoints](#endpoints)
1. [Commands](#commands)
1. [Bonus](#bonus)
1. [Miscellaneous](#miscellaneous)

## Overview
Deploy a WordPress site with Nginx, Redis and MySQL database into Kubernetes cluster. 

Wordpress and MySQL use PersistentVolumes and PersistentVolumeClaims to store data.

## Deliverable
- A log (input and output) of the significant commands ran in the CLI

- Original Kubernetes manifests created

- Original Dockerfiles created

- Configuration files

## Quick-Start
```
- Create secret with mysql root password
$ kubectl apply -f secrets.yaml

- Configure mysql
kubectl apply -f mysql.yaml

- Configure redis and configmap for wordpress
kubectl apply -f redis-config.yaml
kubectl apply -f redis.yaml

- Configure wordpress
kubectl apply -f wordpress.yaml

- Configure nginx
kubectl apply -f nginx-config.yaml
kubectl apply -f nginx.yaml
```

## Endpoints
URL service (nginx):

http://35.232.241.49

http://35.232.241.49/wp-admin

## Commands 

```txt
$ kubectl get pv
NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS    CLAIM                    STORAGECLASS   REASON    AGE
pvc-d862252b-81f4-11e9-baee-42010a80008e   20Gi       RWO            Delete           Bound     default/mysql-pv-claim   standard                 8m
pvc-fd82572f-81f4-11e9-baee-42010a80008e   20Gi       RWO            Delete           Bound     default/wp-pv-claim      standard                 7m

$ kubectl get pvc
NAME             STATUS    VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
mysql-pv-claim   Bound     pvc-d862252b-81f4-11e9-baee-42010a80008e   20Gi       RWO            standard       9m
wp-pv-claim      Bound     pvc-fd82572f-81f4-11e9-baee-42010a80008e   20Gi       RWO            standard       8m

$ kubectl get secret
NAME                  TYPE                                  DATA      AGE
default-token-tchsl   kubernetes.io/service-account-token   3         8d
wordpress-secrets     Opaque                                1         9m

$ kubectl get configmap
NAME           DATA      AGE
nginx-config   1         8m
redis-config   1         8m

$ kubectl get pods
NAME                               READY     STATUS    RESTARTS   AGE
nginx-5cc647887f-d9r9x             1/1       Running   0          8m
nginx-5cc647887f-mrk2w             1/1       Running   0          8m
redis-7ddcf66fd9-rldlj             1/1       Running   0          8m
wordpress-547dcb6d55-dk76l         1/1       Running   0          8m
wordpress-547dcb6d55-fd5w7         1/1       Running   0          8m
wordpress-mysql-6c59cddfc8-x5f74   1/1       Running   0          9m

$ kubectl get svc
NAME              TYPE           CLUSTER-IP    EXTERNAL-IP     PORT(S)        AGE
kubernetes        ClusterIP      10.16.0.1     <none>          443/TCP        8d
nginx             LoadBalancer   10.16.1.14    35.232.241.49   80:32178/TCP   8m
redis             ClusterIP      10.16.6.93    <none>          6379/TCP       8m
wordpress         ClusterIP      10.16.2.184   <none>          9000/TCP       8m
wordpress-mysql   ClusterIP      10.16.1.189   <none>          3306/TCP       9m

$ kubectl get ep
NAME              ENDPOINTS                         AGE
kubernetes        35.222.70.19:443                  8d
nginx             10.12.0.33:80,10.12.0.34:80       8m
redis             10.12.0.30:6379                   9m
wordpress         10.12.0.31:9000,10.12.0.32:9000   9m
wordpress-mysql   10.12.0.29:3306                   9m

$ kubectl get all
NAME                                   READY     STATUS    RESTARTS   AGE
pod/nginx-5cc647887f-d9r9x             1/1       Running   0          8m
pod/nginx-5cc647887f-mrk2w             1/1       Running   0          8m
pod/redis-7ddcf66fd9-rldlj             1/1       Running   0          9m
pod/wordpress-547dcb6d55-dk76l         1/1       Running   0          9m
pod/wordpress-547dcb6d55-fd5w7         1/1       Running   0          9m
pod/wordpress-mysql-6c59cddfc8-x5f74   1/1       Running   0          10m

NAME                      TYPE           CLUSTER-IP    EXTERNAL-IP     PORT(S)        AGE
service/kubernetes        ClusterIP      10.16.0.1     <none>          443/TCP        8d
service/nginx             LoadBalancer   10.16.1.14    35.232.241.49   80:32178/TCP   8m
service/redis             ClusterIP      10.16.6.93    <none>          6379/TCP       9m
service/wordpress         ClusterIP      10.16.2.184   <none>          9000/TCP       9m
service/wordpress-mysql   ClusterIP      10.16.1.189   <none>          3306/TCP       9m

NAME                              DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/nginx             2         2         2            2           8m
deployment.apps/redis             1         1         1            1           9m
deployment.apps/wordpress         2         2         2            2           9m
deployment.apps/wordpress-mysql   1         1         1            1           10m

NAME                                         DESIRED   CURRENT   READY     AGE
replicaset.apps/nginx-5cc647887f             2         2         2         8m
replicaset.apps/redis-7ddcf66fd9             1         1         1         9m
replicaset.apps/wordpress-547dcb6d55         2         2         2         9m
replicaset.apps/wordpress-mysql-6c59cddfc8   1         1         1         10m
```

## Bonus
#### Metrics
For the metrics, I would choose one of the best solutions that integrates well with kubernetes. Prometheus.

Prometheus collects metrics from your objectives at defined time intervals and, if desired, triggers alerts. It works very well in data collection in dynamic environments, which makes it the ideal tool for monitor your Kubernetes, and the services deployed in it, it has many plugins (called exporters).
And for the visualization part I would use Grafana.

#### Logs
For the logs part, I would choose fluentd, this installs a DaemonSet that runs in each kubernetes's node and can send the logs to ES (elasticSearch) nodes and then can be consulted through Kibana easily.
Another similar approach that could be work on GCP is described here:
https://kubernetes.io/docs/tasks/debug-application-cluster/logging-elasticsearch-kibana/


## Miscellaneous
#### Docker images
| Service                   | Image  | Comment                            |
|------------------------|--------------|-----------------------------------|
| Redis       | redis:5.0.5      | https://github.com/docker-library/redis/blob/6845f6d4940f94c50a9f1bf16e07058d0fe4bc4f/5.0/Dockerfile          |
| MySQL         | mysql:5.6           | https://github.com/docker-library/mysql/blob/38510a26f56f429817da7cabf5c772756faa5e55/5.6/Dockerfile       |
| Nginx          | nginx:1.15.12          | https://github.com/nginxinc/docker-nginx/blob/e5123eea0d29c8d13df17d782f15679458ff899e/mainline/stretch/Dockerfile           |
| Wordpress           | neodani/wordpress:1.0          | [Dockerfile](https://gitlab.com/neodani/onna/blob/master/Dockerfile "Dockerfile")           |





