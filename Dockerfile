FROM wordpress:5.2.1-php7.1-fpm

# Install unzip
RUN apt-get update; \
    apt-get install -y --no-install-recommends unzip

# Install WP plugins
RUN curl -L https://downloads.wordpress.org/plugin/redis-cache.1.4.3.zip -o /tmp/redis-cache.1.4.3.zip
RUN unzip /tmp/redis-cache.1.4.3.zip -d /usr/src/wordpress/wp-content/plugins
RUN rm /tmp/redis-cache.1.4.3.zip
